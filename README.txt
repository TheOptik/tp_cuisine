TP fil-rouge

Projet école 1 - la cuisine

Notions vues :

Java, HTML, CSS, JavaScript, Servlets, JSP

Sujet :

Une application est réalisée en JEE. Le code est propre et maintenable, les fichiers bien nommés.

Des cuisiniers nécessitent une application web interne. Celle-ci propose de multiples pages :
- une page d'accueil présente le site (HTML)
- une page permet de chronométrer (JavaScript) : choisir un temps en heurs, minutes et secondes, appuyer sur un bouton, le chronomètre démarre et la page clignote (voire émet un son) lorsque le temps est écoulé. Un bouton additionel permet de réinitialiser le formulaire
- une page permet de calculer une temps de décongélation (Servlet+JSP) : indiquer dans un formulaire une masse en kg, et un bouton d'envoi. Au retour du formulaire, afficher le temps calculé, à l'air libre et en four (respectivement 10h/kg et 12min/kg).
- une page indique les équivalences (Servlet+JSP) : choisir une quantité et une unité (parmis g, kg, litre, pincée, cuillère à café, cuillère à soupe, tasse...), et appuyer sur un bouton d'envoi pour afficher l'équivalent dans toutes les autres unités (pour de l'eau ou un ingrédient similaire)

Toutes les pages contiennent un menu identique pour se rendre vers les autres pages.
Le style graphique rapelle une, un livre de cuisine des années 1950/60 (CSS).

Modalités :

Projet en groupes aléatoires de 3 personnes, 13h de réalisation en 2 itérations de 7h et 6h ; sujet donné en début de première journée, annonce dans la première heure, de la part du groupe, de l'intention du rendu de la 1ère itération, rendu à la 7h de la 1ère itération et à la 13ème heure de la seconde

Groupes :
(Nouhaila), Theo, Gautier, Nicolas
Océane, Tristan, Dorra
Alexis, Océanne, Emile

Rendu :
- Au bout d'une heure : un cours document .txt (100 mots) annonçant le rendu de la 1ère itération
- Au bout de 7h : projet Web Eclipse (1ere itération) exporté dans un .zip
- Au bout de 13h : projet Web Eclipse (2eme itération) exporté dans un .zip

(c) Mickael Blanchard, 2021